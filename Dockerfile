FROM node:12.16.0-alpine3.10

RUN mkdir /app
WORKDIR /app

COPY package* charts/* ./
RUN npm ci

COPY . .

ENV HELM_USER=helm HELM_PASS=pass

CMD ["npm", "start"]

EXPOSE 5000
