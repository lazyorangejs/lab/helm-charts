## Overview

This solution fits for you if:
- you don't want to maintain complex CI/CD pipelines
- you use [Gitlab AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/) pipelines
- you need own [private Helm Charts repository](https://helm.sh/docs/topics/chart_repository/)
- you don't want to configure [Chartmuseum](https://chartmuseum.com/) and Storage backends

## How to run

Just fork this repo to your Gitlab group with connected Kubernetes cluter :) 
Add **K8S_SECRET_HELM_USER** and **K8S_SECRET_HELM_PASS** to [Gitlab CI/CD variables](https://gitlab.com/help/ci/variables/README#variables)
in order to update access credentials to helm repo.

### How to build repo index for Helm charts?

```bash
# Helm 3
$ helm repo add stable https://kubernetes-charts.storage.googleapis.com/
$ helm repo update
$ helm dep build ./charts/auto-deploy-app
$ helm package -d ./charts ./charts/auto-deploy-app
$ helm repo index ./charts
$ # push to master branch if you know what you do :)
$ git push origin master 
$ # or push to own branch and open merge request :)
```

### How to deploy your own private helm chart repository?

It works well with Helm v2 while you use Kubernetes v1.15 and Gitlab's AutoDevOps chart.

You can't install it via Helm 3 due to PostgreSQL dependency from Gitlab's AutoDevOps chart which doesn't support Kubernetes v1.16+.
You still can use Kubernetes v1.16+, but keep in mind that you can't install this chart by Helm v3.

To deploy to your own Kubernetes cluster, just fork this repo :)

### Add helm repository
```bash
$ helm repo add --username=helm --password=pass lo https://le-19036308.lab.lazyorange.xyz
$ helm repo update
```

Output:
```bash
"lo" has been added to your repositories
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "lo" chart repository
...Successfully got an update from the "gitlab" chart repository
...Successfully got an update from the "stable" chart repository
Update Complete. ⎈ Happy Helming!⎈ 
```

```bash
$ # Helm 2
$ helm search lo/auto-deploy-app
$ # Helm 3
$ helm search repo lo/auto-deploy-app
```

### Install httpbin server from our helm repo
```bash
$ helm upgrade -f .gitlab/httpbin-values.yaml --namespace=default --install httpbin lo/auto-deploy-app
```

